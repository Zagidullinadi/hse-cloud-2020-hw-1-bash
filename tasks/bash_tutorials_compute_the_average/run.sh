#!/bin/bash

read n
sum=0
for val in $(cat /dev/stdin)
  do
    sum=$(echo $sum+$val | bc )
  done
echo "scale=3; $sum/$n" | bc
